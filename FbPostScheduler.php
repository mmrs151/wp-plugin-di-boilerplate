<?php
defined( 'ABSPATH' ) OR exit;
/*
Plugin Name: Facebook Post Scheduler
Description: Schedule post for facebook in advance
Version: 1.0.0
Author: mmrs151
Author URI: https://profiles.wordpress.org/mmrs151/#content-plugins
License: GPL-2.0+
*/

use FPS\DI\PluginContainer;

spl_autoload_register( 'fps_autoloader' );
function fps_autoloader( $class_name ) {
    if ( false !== strpos( $class_name, 'FPS' ) ) {
        $classes_dir = realpath( plugin_dir_path( __FILE__ ) ) . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR;
        $class_file = str_replace( '\\', DIRECTORY_SEPARATOR, $class_name ) . '.php';
        require_once $classes_dir . $class_file;
    }
}

add_action( 'plugins_loaded', 'fps_init' ); // Hook initialization function
function fps_init() {

    $container = new PluginContainer(); // Create container
    $container['path'] = realpath( plugin_dir_path( __FILE__ ) ) . DIRECTORY_SEPARATOR;
    $container['url'] = plugin_dir_url( __FILE__ );
    $container['version'] = '1.0.0';

    $container['loader'] = function ( $container ) {
        return new FPS\models\Loader();
    };

    $container->run();
}

##########################################################
# DEACTIVATION #
##########################################################
register_deactivation_hook( __FILE__, 'uninstallFPS' );
function uninstallFPS() {
}