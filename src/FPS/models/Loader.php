<?php
namespace FPS\models;

class Loader
{
    public function run()
    {
        add_action( 'wp_enqueue_scripts', array( $this, 'load_scripts_and_styles' ) );
    }

    public function load_scripts_and_styles()
    {
        // load css
        wp_enqueue_style( 'fps.css',
            plugins_url( '../../assets/css/fps.css' , __FILE__ ),
            array(),
            filemtime( plugin_dir_path( __FILE__ ) . '../../assets/css/fps.css' ) );

        // load js
        wp_register_script( 'bpmmm',
            plugins_url( '../../assets/js/fps.js' , __FILE__ ),
            array( 'jquery' ),
            '1.0.0', true );

        $protocol = isset( $_SERVER['HTTPS'] ) ? 'https://' : 'http://';

        $params = array(
            // Get the url to the admin-ajax.php file using admin_url()
            'ajaxurl' => admin_url( 'admin-ajax.php', $protocol ),
        );

        wp_enqueue_script('fps');
        wp_localize_script( 'fps', 'fps_params', $params );
    }
}